# Weight Converter App

## INSTRUCTIONS
1. Fork this repository
2. Clone the forked repository.
3. Read through the existing code that is already on here. 
4. Make sure you go through all the files!
5. Include any necessary code that is needed to complete the tasks.
6. Code may be deleted if desired.


## TODO

* As a user, when I enter a number, I should get the corresponding conversions so that I can get the correct calculations in grams, kilograms, and ounces.
* As a user, I would like the style of the app should be different, so that I can see the results in a distinct display.
* As a user, when the number entry is updated, I would like to see the updates in the conversions as well.

Here's a sample video:

![Sample Video](weight-convert-demo.mov)

## FEATURE

* BONUS: As a user, I would like a dropdown menu so that I can have the option(s) select enter different types of weights (view bonus video demo)

Here's the bonus demo video:

![Sample Video](weight-converter-bonus.mov)
